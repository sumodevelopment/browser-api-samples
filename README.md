# Browser API demos

## Geolocation
Get the current location

## Play Sound
Play a basic sine wave for 1500ms

## Fetch todos
Use the browser fetch() api to make an HTTP GET Request.
