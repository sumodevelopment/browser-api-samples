const btnGetLocation = document.getElementById('btn-get-location');
const btnPlaySound = document.getElementById('btn-play-sound');
const btnFetchTodos = document.getElementById('btn-fetch-todos');
const elStatus = document.getElementById('status');

btnGetLocation.addEventListener('click', getCurrentLocation);
btnFetchTodos.addEventListener('click', fetchTodos);
btnPlaySound.addEventListener('click', playSound);

function getCurrentLocation() {

    if (!navigator || !navigator.geolocation) {
        setStatus('Your browser does not support geolocation.');
        return;
    }

    setStatus('Getting location...');
    
    const onSuccess = function(position) {
        const { coords } = position;
        setStatus(`lng: ${ coords.longitude }, lat: ${ coords.latitude }`);
    }

    const onError = function(error) {
        setStatus(error.message);
    }

    navigator.geolocation.getCurrentPosition(onSuccess, onError);
}

function fetchTodos() {
    
    elStatus.innerText = 'Fetching todos...';

    const onSuccess = function(todos) {
        
        const todoHTML = todos.map(function(todo) {
            return `<li>${todo.title}</li>`;
        }).join('')
        
        setStatus(`<ul>${todoHTML}</ul>`);
    }

    const onError = function(error) {
        setStatus(error.message);
    }

    fetch('https://jsonplaceholder.typicode.com/todos')
        .then(response => response.json())
        .then(onSuccess)
        .catch(onError);
}

function playSound() {
    setStatus('Playing sine wave...');
    const context = new AudioContext();
    const oscillator = context.createOscillator();
    oscillator.type = "sine";
    oscillator.connect(context.destination);
    oscillator.start();
    setTimeout(function() {
        oscillator.stop();
        setStatus('Stopped playing sound');
    }, 1500);
}

function setStatus(content) {
    elStatus.innerHTML = content;
}
